package controller;

import exception.AlertTypeException;
import exception.ChartTypeException;
import javafx.application.Platform;
import javafx.beans.value.ObservableValue;
import javafx.collections.ObservableList;
import javafx.embed.swing.SwingFXUtils;
import javafx.fxml.FXML;
import javafx.scene.Node;
import javafx.scene.SnapshotParameters;
import javafx.scene.chart.AreaChart;
import javafx.scene.chart.Chart;
import javafx.scene.chart.PieChart;
import javafx.scene.chart.XYChart;
import javafx.scene.control.*;
import javafx.scene.control.cell.TextFieldTableCell;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseButton;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import javafx.util.StringConverter;
import javafx.util.converter.DefaultStringConverter;
import javafx.util.converter.NumberStringConverter;
import model.ChartData;
import model.Data;
import util.factory.ChartFactoryImpl;
import util.factory.DialogFactory;
import util.factory.ParserFactoryImpl;
import util.helper.ChartCSSHelper;
import util.helper.ConstHelper;
import util.helper.FileHelper;
import util.parser.CSVParserImpl;
import util.parser.IBaseParser;

import javax.imageio.ImageIO;
import java.io.File;
import java.io.IOException;
import java.text.MessageFormat;
import java.util.Date;
import java.util.Optional;
import java.util.Set;
import java.util.function.Function;

/**
 * Main Frame Controller.
 *
 * Created by Andrzej Olkiewicz on 2016-04-24.
 */
public class MainFrameController {

    @FXML
    BorderPane borderPane;

    @FXML
    MenuItem menuFileClose;

    @FXML
    MenuItem menuFileOpen;

    @FXML
    MenuItem menuFileSave;

    @FXML
    MenuItem menuHelpAbout;

    @FXML
    TableView<Data> chartDataTableView;

    @FXML
    TextField chartTitleTF;

    @FXML
    TextField chartAxisXTitleTF;

    @FXML
    TextField chartAxisYTitleTF;

    // Radio Buttons
    @FXML
    ToggleGroup chartTypeGroup = new ToggleGroup();

    @FXML
    RadioButton barChartRadioBtn;

    @FXML
    RadioButton pieChartRadioBtn;

    @FXML
    RadioButton lineChartRadioBtn;

    @FXML
    RadioButton areaChartRadioBtn;

    @FXML
    RadioButton bubbleChartRadioBtn;

    @FXML
    RadioButton scatterChartRadioBtn;
    // End.

    @FXML
    ComboBox<String> chartSeriesComboBox;

    @FXML
    VBox ChartVBox;

    @FXML
    ColorPicker seriesColorPicker;

    @FXML
    ColorPicker axisColorPicker;

    @FXML
    ColorPicker chartTitleColorPicker;

    @FXML
    ColorPicker plotBackgroundColorPicker;

    @FXML
    ColorPicker verticalGridLinesColorPicker;

    @FXML
    ColorPicker horizontalGridLinesColorPicker;

    @FXML
    ColorPicker alternativeRowFillColorPicker;

    private Chart chart;
    private ChartData chartData;

    /**
     * Initialize Radio Buttons Action Listeners
     */
    private void initializeRadioButtonsActionListeners(){
        barChartRadioBtn.setOnAction(event -> {
            try {
                if(chartData != null) {
                    chart = new ChartFactoryImpl().getChart(ConstHelper.BarChart, chartData);

                    if(chart != null) {
                        initializeChart(chart);
                    }
                }
            } catch (AlertTypeException e) {
                e.printStackTrace();
            }
        });

        pieChartRadioBtn.setOnAction(event -> {
            try {
                if(chartData != null) {
                    chart = new ChartFactoryImpl().getChart(ConstHelper.PieChart, chartData);

                    if(chart != null) {
                        initializeChart(chart);
                    }
                }
            } catch (AlertTypeException e) {
                e.printStackTrace();
            }
        });

        lineChartRadioBtn.setOnAction(event -> {
            try {
                if(chartData != null) {
                    chart = new ChartFactoryImpl().getChart(ConstHelper.LineChart, chartData);

                    if(chart != null) {
                        initializeChart(chart);
                    }
                }
            } catch (AlertTypeException e) {
                e.printStackTrace();
            }
        });

        areaChartRadioBtn.setOnAction(event -> {
            try {
                if(chartData != null) {
                    chart = new ChartFactoryImpl().getChart(ConstHelper.AreaChart, chartData);

                    if(chart != null) {
                        initializeChart(chart);
                    }
                }
            } catch (AlertTypeException e) {
                e.printStackTrace();
            }
        });

        bubbleChartRadioBtn.setOnAction(event -> {
            try {
                if(chartData != null) {
                    chart = new ChartFactoryImpl().getChart(ConstHelper.BubbleChart, chartData);

                    if(chart != null) {
                        initializeChart(chart);
                    }
                }
            } catch (AlertTypeException e) {
                e.printStackTrace();
            }
        });

        scatterChartRadioBtn.setOnAction(event -> {
            try {
                if(chartData != null) {
                    chart = new ChartFactoryImpl().getChart(ConstHelper.ScatterChart, chartData);

                    if(chart != null) {
                        initializeChart(chart);
                    }
                }
            } catch (AlertTypeException e) {
                e.printStackTrace();
            }
        });
    }

    /**
     * Initialize Chart Listeners, save as Image.
     *
     * @param stage
     */
    private void initializeChartListeners(Stage stage) {
        // save chart context menu
        final ContextMenu saveChartContextMenu = new ContextMenu();
        MenuItem saveChartMenuItem = new MenuItem("Save chart as image...");
        saveChartContextMenu.getItems().add(saveChartMenuItem);

        saveChartMenuItem.setOnAction(e -> {
            FileChooser saveFileChooser = getSaveChartDialog();

            Optional<File> saveFile = Optional.ofNullable(saveFileChooser.showSaveDialog(stage));

            if(saveFile.isPresent()){
                try {
                    ImageIO.write(SwingFXUtils.fromFXImage(chart.snapshot(new SnapshotParameters(), null),
                            null), "png", saveFile.get());
                } catch (IOException e1) {
                    e1.printStackTrace();
                }
            }
        });

        chart.setOnMouseClicked(event -> {
            if(event.getButton() == MouseButton.SECONDARY)
                saveChartContextMenu.show(chart, event.getScreenX(), event.getScreenY());
        });
    }

    /**
     * Initialize TableView Listeners
     *
     * @param chartData
     */
    private void initializeTableViewListeners(ChartData chartData) {
        chartDataTableView.getColumns().clear();
        chartDataTableView.getItems().clear();

        DefaultStringConverter def = new DefaultStringConverter();
        NumberStringConverter num = new NumberStringConverter();

        chartDataTableView.getColumns().add(createCol("Name", Data::nameProperty, def, false));

        if(chartData.getSeries().size() != 0) {
            for (int i = 0; i < chartData.getDataValuesSize(); i++) {
                final int finalI = i;
                String name = chartData.getSeries().get(finalI);

                chartDataTableView.getColumns().add(createCol(name,
                        data -> data.getProperty(finalI),
                        num, true));
            }
        } else {
            for (int i = 0; i < chartData.getDataValuesSize(); i++) {
                final int finalI = i;
                String name = "Value";

                chartDataTableView.getColumns().add(createCol(name,
                        data -> data.getProperty(finalI),
                        num, true));
            }
        }
    }

    /**
     * Initialize Color Picker Listeners
     */
    private void initializeColorPicker(){
        seriesColorPicker.setOnAction(event -> {
            try {
                updateChartColor(chart);
            } catch (AlertTypeException e) {
                e.printStackTrace();
            }
            updateChartLegendColorFromItemName(chart, chartSeriesComboBox.getSelectionModel().getSelectedItem(), seriesColorPicker.getValue());
        });

        axisColorPicker.setOnAction(event -> {
            MessageFormat mf = new MessageFormat(ConstHelper.axisLabelColor);
            String defColor = mf.format(new Object[]{toRGBCode(axisColorPicker.getValue())});

            for(Node n: chart.lookupAll(ConstHelper.axisLabel)) {
                n.setStyle(defColor);
            }
        });

        chartTitleColorPicker.setOnAction(event -> {
            MessageFormat mf = new MessageFormat(ConstHelper.chartTitleColor);
            String defColor = mf.format(new Object[]{toRGBCode(chartTitleColorPicker.getValue())});

            for(Node n: chart.lookupAll(ConstHelper.chartTitle)) {
                n.setStyle(defColor);
            }
        });

        plotBackgroundColorPicker.setOnAction(event ->  {
            MessageFormat mf = new MessageFormat(ConstHelper.plotBackGroundColor);
            String defColor = mf.format(new Object[]{toRGBCode(plotBackgroundColorPicker.getValue())});

            for(Node n: chart.lookupAll(ConstHelper.plotBackGround)) {
                n.setStyle(defColor);
            }
        });

        verticalGridLinesColorPicker.setOnAction(event ->  {
            MessageFormat mf = new MessageFormat(ConstHelper.FX_STROKE_0);
            String defColor = mf.format(new Object[]{toRGBCode(verticalGridLinesColorPicker.getValue())});

            for(Node n: chart.lookupAll(ConstHelper.verticalGridLines)) {
                System.out.println("Tutaj jestem!!!");
                n.setStyle(defColor);
            }
        });

        horizontalGridLinesColorPicker.setOnAction(event ->  {
            MessageFormat mf = new MessageFormat(ConstHelper.FX_STROKE_0);
            String defColor = mf.format(new Object[]{toRGBCode(horizontalGridLinesColorPicker.getValue())});

            for(Node n: chart.lookupAll(ConstHelper.horizontalGridLines)) {
                n.setStyle(defColor);
            }
        });

        alternativeRowFillColorPicker.setOnAction(event ->  {
            MessageFormat mf = new MessageFormat(ConstHelper.FX_FILL_0);
            String defColor = mf.format(new Object[]{toRGBCode(alternativeRowFillColorPicker.getValue())});

            for(Node n: chart.lookupAll(ConstHelper.alternativeRowFill)) {
                n.setStyle(defColor);
            }
        });
    }

    /**
     * Init before start
     */
    @FXML
    private void initialize() {
        initializeRadioButtonsActionListeners();
        initializeColorPicker();
    }


    /**
     * Open File.
     *
     * @throws Exception
     */
    public void openFile() throws Exception {
        Stage stage = (Stage) borderPane.getScene().getWindow();

        // FileChooser, open File
        FileChooser openFileChooser = getOpenDialog();

        Optional<File> openFile = Optional.ofNullable(openFileChooser.showOpenDialog(stage));

        if (openFile.isPresent()) {

            // Get parser and Parse data to chartData object
            IBaseParser fileParser = new ParserFactoryImpl().getFileParser(FileHelper.getFileExtension(openFile.get().getName()));

            if (fileParser == null)
                return;

            chartData = fileParser.parseData(openFile.get(), getChartType());

            if (chartData == null)
                return;

            // get Chart based on ChartFactory
            chart = new ChartFactoryImpl().getChart(getChartType(), chartData);

            if (chart == null)
                return;

            // init tableview
            initializeTableViewListeners(chartData);

            // add Items to TableView
            chartDataTableView.setItems(chartData.getData());

            // disable TextFields if not available
            disableAxisTitleTF(chart);

            // initializeListeners for saveSnapshot of Chart
            initializeChartListeners(stage);

            // add series name to combobox
            chartSeriesComboBox.getItems().clear();

            if(chartData.getSeriesSize() != 0)
                chartSeriesComboBox.getItems().addAll(chartData.getSeries());
            else
                for(int i = 0; i < chartData.getData().size(); i++)
                    chartSeriesComboBox.getItems().add(chartData.getData().get(i).getName());

            // add Chart to VBox
            addChartToVBox();
        }
    }

    /**
     * Save File.
     *
     * @throws AlertTypeException
     */
    public void saveFile() throws AlertTypeException {
        Stage stage = (Stage) borderPane.getScene().getWindow();

        // FileChooser, open File
        FileChooser openFileChooser = getSaveDataDialog();

        Optional<File> saveFile = Optional.ofNullable(openFileChooser.showSaveDialog(stage));

        if(saveFile.isPresent()){
            CSVParserImpl.getInstance().saveData(saveFile.get(), chartData);
            System.out.println("Done...");
        }
    }

    /**
     * Chart title Text field handler.
     *
     * @param event
     */
    public void chartTitleTextFieldHandler(KeyEvent event) {
        if (event.getCode() == KeyCode.ENTER) {
            chart.setTitle(chartTitleTF.getText());
         }
    }

    /**
     * Chart Axis X Text Field handler.
     *
     * @param event
     */
    public void chartAxisXTextFieldHandler(KeyEvent event) {
        if (event.getCode() == KeyCode.ENTER) {
            XYChart bar = (XYChart) chart;
            bar.getXAxis().setLabel(chartAxisXTitleTF.getText());
        }
    }

    /**
     * Chart Axis Y Text Field handler.
     *
     * @param event
     */
    public void chartAxisYTextFieldHandler(KeyEvent event) {
        if (event.getCode() == KeyCode.ENTER) {
            XYChart bar = (XYChart) chart;
            bar.getYAxis().setLabel(chartAxisYTitleTF.getText());
        }
    }

    /**
     * Close window alert.
     */
    public void closeWindow() {
        Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
        alert.setTitle("Confirmation Dialog");
        alert.setHeaderText(null);
        alert.setContentText("Are you sure that you want close application?");

        Optional<ButtonType> result = alert.showAndWait();
        if (result.get() == ButtonType.OK){
            Platform.exit();
        } else {
            return;
        }
    }

    /**
     * About app information alert.
     */
    public void menuHelpAboutClickedHandler() {
        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setTitle("About Chart Generator");
        alert.setHeaderText("Chart Generator 1.0");
        alert.setContentText("Product Version:\n" +
                "Chart Generator 1.0\n\n" +
                "Version Date:\n" + new Date().toString() + "\n\n" +
                "Authors:\n" +
                "Andrzej Olkiewicz\n" +
                "Damian Nowak\n" +
                "Maciej Pawlikowski");

        alert.showAndWait();
    }

    /**
     * Obtain Chart type
     *
     * @return chart type
     */
    // Common methods
    private String getChartType(){
        RadioButton btn = (RadioButton) chartTypeGroup.getSelectedToggle();

        switch(btn.getText()) {
            case "Bar Chart":
                return ConstHelper.BarChart;
            case "Pie Chart":
                return ConstHelper.PieChart;
            case "Line Chart":
                return ConstHelper.LineChart;
            case "Area Chart":
                return ConstHelper.AreaChart;
            case "Bubble Chart":
                return ConstHelper.BubbleChart;
            case "Scatter Chart":
                return ConstHelper.ScatterChart;
            default:
                return "";
        }
    }

    /**
     * Shows open file dialog.
     *
     * @return open file chooser.
     */
    private FileChooser getOpenDialog(){
        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle("Open Resource File");
        fileChooser.getExtensionFilters().addAll(new FileChooser.ExtensionFilter("CSV Files", "*.csv"));

        return fileChooser;
    }

    /**
     * Shows save file dialog.
     *
     * @return save file chooser.
     */
    private FileChooser getSaveDataDialog(){
        FileChooser fileChooser = new FileChooser();
        fileChooser.getExtensionFilters().addAll(new FileChooser.ExtensionFilter("CSV Files", "*.csv"));
        fileChooser.setTitle("Save Data");

        return fileChooser;
    }

    /**
     * Shows save chart as image dialog.
     *
     * @return save chart as image dialog.
     */
    private FileChooser getSaveChartDialog(){
        FileChooser fileChooser = new FileChooser();
        fileChooser.getExtensionFilters().addAll(new FileChooser.ExtensionFilter("PNG image", "*.png"));
        fileChooser.setTitle("Save Image");

        return fileChooser;
    }

    /**
     * Add chart to VBox.
     */
    private void addChartToVBox(){
        // add Chart to VBox
        ObservableList<Node> chartVBoxNodes = ChartVBox.getChildren();

        if(chartVBoxNodes != null && chart != null){
            chart.setAnimated(false);

            chartVBoxNodes.clear();
            chartVBoxNodes.add(chart);
        }
    }

    /**
     * Disable axis X and Y title textfields, when not available.
     * @param chart chart instance
     */
    private void disableAxisTitleTF(Chart chart) {
        boolean disableAxisTitleTF = chart instanceof PieChart;

        chartAxisXTitleTF.setDisable(disableAxisTitleTF);
        chartAxisYTitleTF.setDisable(disableAxisTitleTF);
    }

    /**
     * Initialize chart listeners
     *
     * @param chart chart instance
     */
    private void initializeChart(Chart chart){
        Stage stage = (Stage) borderPane.getScene().getWindow();

        // initializeListeners for saveSnapshot of Chart
        initializeChartListeners(stage);

        // disable TextFields if not available
        disableAxisTitleTF(chart);

        addChartToVBox();
    }

    /**
     * Common function, format Color to String value.
     *
     * @param color color instance
     * @return color parsed to string value
     */
    private static String toRGBCode( Color color )
    {
        return String.format( "#%02X%02X%02X",
                (int)( color.getRed() * 255 ),
                (int)( color.getGreen() * 255 ),
                (int)( color.getBlue() * 255 ) );
    }

    /**
     * Create TableColumn
     *
     * @param title Column title
     * @param propertySelector Property selector
     * @param converter Value converter
     * @param editable Is Column editable
     * @param <S>
     * @param <T>
     * @return
     */
    private <S, T> TableColumn<S,T> createCol(String title,
                                              Function<S,ObservableValue<T>> propertySelector,
                                              StringConverter<T> converter,
                                              boolean editable) {

        TableColumn<S,T> col = new TableColumn<>(title);
        col.setEditable(editable);
        col.setCellFactory(TextFieldTableCell.forTableColumn(converter));
        col.setCellValueFactory(cellData -> propertySelector.apply(cellData.getValue()));

        return col;
    }

    /**
     * Update chart colors
     *
     * @param chart chart instance
     * @throws AlertTypeException
     */
    private void updateChartColor(Chart chart) throws AlertTypeException {
        try {
            MessageFormat mf2 = new MessageFormat(ChartCSSHelper.selectColorDef(getChartType()));
            String defColor = mf2.format(new Object[]{chartSeriesComboBox.getSelectionModel().getSelectedIndex()});

            MessageFormat mf = new MessageFormat(ChartCSSHelper.selectColorFill(getChartType()));
            String colorFill = mf.format(new Object[]{toRGBCode(seriesColorPicker.getValue())});

            System.out.println("ColorFill: " + colorFill);
            System.out.println("defColor: " + defColor);

            for(Node n: chart.lookupAll(defColor)) {
                n.setStyle(colorFill);
            }

            if(chart instanceof AreaChart){

                mf2 = new MessageFormat(ChartCSSHelper.selectAreaFillColorDef(getChartType()));
                defColor = mf2.format(new Object[]{chartSeriesComboBox.getSelectionModel().getSelectedIndex()});

                mf = new MessageFormat(ChartCSSHelper.selectAreaFillColor(getChartType()));
                colorFill = mf.format(new Object[]{toRGBCode(seriesColorPicker.getValue())});

                for(Node n: chart.lookupAll(defColor)) {
                    n.setStyle(colorFill);
                }
            }

        } catch (ChartTypeException exc){
            DialogFactory.showDialog(Alert.AlertType.ERROR, exc.getMessage());
            return;
        }
    }

    /**
     * Update chart legend color
     *
     * @param chart chart instance
     * @param itemToUpdate legend item to update
     * @param legendColor new legend color
     */
    private void updateChartLegendColorFromItemName(Chart chart, String itemToUpdate, Color legendColor){
        Set<Node> legendItems = chart.lookupAll("Label.chart-legend-item");
        if(legendItems.isEmpty()){ return; }
        String styleString = "-fx-background-color:" + toRGBCode(legendColor) + ";";
        for(Node legendItem : legendItems){
            Label legendLabel = (Label) legendItem;
            Node legend = legendLabel.getGraphic(); //The legend icon (circle by default)

            if(legend != null && legendLabel.getText().equals(itemToUpdate)){
                legend.setStyle(styleString);
            }
        }
    }
}
