package util.helper;

/**
 * Constant values Helper Interface.
 *
 * Created by Andrzej Olkiewicz on 2016-05-24.
 */
public interface ConstHelper {

    // File extensions
    String CSVExtension = "csv";

    // Chart Types
    String BarChart = "BarChart";
    String PieChart = "PieChart";
    String LineChart = "LineChart";
    String AreaChart = "AreaChart";
    String BubbleChart = "BubbleChart";
    String ScatterChart = "ScatterChart";

    // CSS Chart commands

    // bar chart
    String DefColorBarChart = ".default-color{0}.chart-bar";
    String DefColorBarChartFill = "-fx-bar-fill: {0}";

    // pie chart
    String DefColorPieChart = ".default-color{0}.chart-pie";
    String DefColorPieChartFill = "-fx-pie-color: {0}";

    // line chart
    String DefColorLineChart = ".default-color{0}.chart-series-line";

    // area Chart
    String DefColorAreaChartLine = ".default-color{0}.chart-series-area-line";
    String DefColorAreaChartLineFill = "-fx-stroke: {0}";

    String DefColorAreaChartFill = ".default-color{0}.chart-series-area-fill";

    // bubble chart
    String DefColorBubbleChart = ".default-color{0}.chart-bubble";
    String DefColorBubbleChartFill = "-fx-bubble-fill: {0}";

    // scatter chart
    String DefColorScatterChart = ".default-color{0}.chart-symbol";
    String DefColorScatterChartFill = "-fx-background-color: {0}";

    // Common
    String axisLabel = ".axis-label";
    String axisLabelColor = "-fx-text-fill: {0};";

    String chartTitle = ".chart-title";
    String chartTitleColor = "-fx-text-fill: {0};";

    String plotBackGround = ".chart-plot-background";
    String plotBackGroundColor = "-fx-background-color: {0};";

    String verticalGridLines = ".chart-vertical-grid-lines";
    String horizontalGridLines = ".chart-horizontal-grid-lines";
    String alternativeRowFill = ".chart-alternative-row-fill";

    String FX_FILL_0 = "-fx-fill: {0}";
    String FX_STROKE_0 = "-fx-stroke: {0}";
}
