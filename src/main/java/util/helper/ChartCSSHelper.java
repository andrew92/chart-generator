package util.helper;

import exception.ChartTypeException;

/**
 * Chart CSS Helper class.
 *
 * Created by Andrzej Olkiewicz on 2016-06-09.
 */
public class ChartCSSHelper {

    /**
     * Returns CSS for Color Definition.
     *
     * @param type Chart type
     * @return CSS command
     * @throws ChartTypeException
     */
    public static String selectColorDef(String type) throws ChartTypeException {
        switch(type) {
            case ConstHelper.BarChart:
                return ConstHelper.DefColorBarChart;
            case ConstHelper.PieChart:
                return ConstHelper.DefColorPieChart;
            case ConstHelper.LineChart:
                return ConstHelper.DefColorLineChart;
            case ConstHelper.AreaChart:
                return ConstHelper.DefColorAreaChartLine;
            case ConstHelper.BubbleChart:
                return ConstHelper.DefColorBubbleChart;
            case ConstHelper.ScatterChart:
                return ConstHelper.DefColorScatterChart;
            default:
                throw new ChartTypeException("Chart type: " + type + " is invalid");
        }
    }

    /**
     * Returns CSS for Color Area Fill Definition
     *
     * @param type Chart type
     * @return CSS command
     * @throws ChartTypeException
     */
    public static String selectAreaFillColorDef(String type) throws ChartTypeException {
        switch(type) {
            case ConstHelper.AreaChart:
                return ConstHelper.DefColorAreaChartFill;
            default:
                throw new ChartTypeException("Chart type: " + type + " is invalid");
        }
    }

    /**
     * Returns CSS for Color Area Fill Color Definition
     *
     * @param type Chart type
     * @return CSS command
     * @throws ChartTypeException
     */
    public static String selectAreaFillColor(String type) throws ChartTypeException {
        switch(type) {
            case ConstHelper.AreaChart:
                return ConstHelper.FX_FILL_0;
            default:
                throw new ChartTypeException("Chart type: " + type + " is invalid");
        }
    }

    /**
     * Returns CSS for Color Fill Definition.
     *
     * @param type Chart type
     * @return CSS command
     * @throws ChartTypeException
     */
    public static String selectColorFill(String type) throws ChartTypeException {
        switch(type) {
            case ConstHelper.BarChart:
                return ConstHelper.DefColorBarChartFill;
            case ConstHelper.PieChart:
                return ConstHelper.DefColorPieChartFill;
            case ConstHelper.LineChart:
                return ConstHelper.FX_STROKE_0;
            case ConstHelper.AreaChart:
                return ConstHelper.DefColorAreaChartLineFill;
            case ConstHelper.BubbleChart:
                return ConstHelper.DefColorBubbleChartFill;
            case ConstHelper.ScatterChart:
                return ConstHelper.DefColorScatterChartFill;
            default:
                throw new ChartTypeException("Chart type: " + type + " is invalid");
        }
    }
}
