package util.helper;

/**
 * File Helper class
 *
 * Created by Andrzej Olkiewicz on 2016-05-15.
 */
public class FileHelper {

    /**
     * Returns file extension based on passed as argument full file name.
     *
     * @param fileName Full file name (with extension).
     * @return File Extension
     */
    public static String getFileExtension(String fileName){
        String extension = "";

        int i = fileName.lastIndexOf('.');
        if (i > 0) {
            extension = fileName.substring(i+1);
        }

        return extension;
    }
}
