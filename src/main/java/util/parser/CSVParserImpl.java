package util.parser;

import exception.AlertTypeException;
import exception.ChartDataException;
import javafx.beans.property.Property;
import javafx.scene.control.Alert;
import model.ChartData;
import model.Data;
import util.factory.DialogFactory;
import util.helper.ConstHelper;

import java.io.*;

/**
 * CSV Parser Implementation. Singleton.
 *
 * Created by Andrzej Olkiewicz on 2016-05-15.
 */
public class CSVParserImpl implements IBaseParser {
    private static final String CSVSeparator = ",";
    private ChartData chartData;

    private CSVParserImpl(){}

    private static class SingletonHelper{
        private static final CSVParserImpl csvParserSingleton = new CSVParserImpl();
    }

    /**
     * Returns CSV Parser instance.
     *
     * @return CSVParser instance.
     */
    public static CSVParserImpl getInstance(){
        return SingletonHelper.csvParserSingleton;
    }

    /**
     * Returns ready to used parsed data.
     *
     * @param data File with data
     * @param chartType Chart type, defines file format.
     * @return Parsed data, Chart Data instance.
     * @throws IOException
     * @throws AlertTypeException
     */
    @Override
    public ChartData parseData(File data, String chartType) throws IOException, AlertTypeException {
        switch(chartType){
            case ConstHelper.BarChart:
                return parseDataWhenSeriesArePresent(data);
            case ConstHelper.PieChart:
                return parseDataWhenSeriesAreNotPresent(data);
            case ConstHelper.LineChart:
                return parseDataWhenSeriesArePresent(data);
            case ConstHelper.AreaChart:
                return parseDataWhenSeriesArePresent(data);
            case ConstHelper.BubbleChart:
                return parseDataWhenSeriesArePresent(data);
            case ConstHelper.ScatterChart:
                return parseDataWhenSeriesArePresent(data);
            default:
                return null;
        }
    }

    /**
     * Save data to file in correct to future use format
     *
     * @param saveTo File to be chart data saved to.
     * @param chartData Chart data.
     * @throws AlertTypeException
     */
    @Override
    public void saveData(File saveTo, ChartData chartData) throws AlertTypeException {
        if(chartData == null)
            return;

        PrintWriter printWriter = null;

        try {
            printWriter = new PrintWriter(saveTo);

            StringBuilder stringBuilder = new StringBuilder();

            String str = "";

            // append series if available
            for(int i = 0; i < chartData.getSeriesSize(); i++){
                if(i != chartData.getSeriesSize() - 1)
                    stringBuilder.append(chartData.getSeries().get(i)).append(",");
                else {
                    stringBuilder.append(chartData.getSeries().get(i)).append("\n");
                }

                stringBuilder.append(str);
            }

            // append data
            for(Data data: chartData.getData()){
                // append data name
                stringBuilder.append(data.getName()).append(",");

                // append data values
                int counter = 0;
                for(Property<Number> number: data.getValues()) {
                    counter++;

                    if(counter == data.getValues().size())
                        stringBuilder.append(number.getValue()).append("\n");
                    else
                        stringBuilder.append(number.getValue()).append(",");

                }
            }

            //System.out.println(stringBuilder.toString());
            printWriter.write(stringBuilder.toString());
            printWriter.close();
        } catch (FileNotFoundException e) {
            DialogFactory.showDialog(Alert.AlertType.ERROR, e.getMessage());
            e.printStackTrace();
        } finally {
            if(printWriter != null)
                printWriter.close();
        }
    }

    /**
     * Returns parsed data when Chart Data have any series e.g. Bar Chart.
     *
     * @param data File with data to be parsed.
     * @return Parsed data
     * @throws IOException
     * @throws AlertTypeException
     */
    private ChartData parseDataWhenSeriesArePresent(File data) throws IOException, AlertTypeException {
        chartData = new ChartData();

        BufferedReader bufferedReader = new BufferedReader(new FileReader(data));

        String line;
        try {
            boolean firstLine = true;
            while ((line = bufferedReader.readLine()) != null) {
                String[] splittedLine = line.split(CSVSeparator);

                if (firstLine) {
                    chartData.setSeries(splittedLine);
                    firstLine = false;
                    continue;
                }

                if (splittedLine.length != chartData.getSeriesSize() + 1) {
                    throw new ChartDataException("Incorrect input data, expected " + (chartData.getSeriesSize() + 1) + " but there was " + splittedLine.length);
                }

                chartData.setData(splittedLine);
                printLine(splittedLine);
            }
        } catch (ChartDataException exc) {
            DialogFactory.showDialog(Alert.AlertType.ERROR, exc.getMessage());
            return null;
        }

        return chartData;
    }

    /**
     * Returns parsed data when Chart Data have any series e.g. Pie Chart.
     *
     * @param data File with data to be parsed.
     * @return Parsed data
     * @throws IOException
     * @throws AlertTypeException
     */
    private ChartData parseDataWhenSeriesAreNotPresent(File data) throws IOException, AlertTypeException {
        chartData = new ChartData();

        BufferedReader bufferedReader = new BufferedReader(new FileReader(data));

        String line;
        try {
            while ((line = bufferedReader.readLine()) != null) {
                String[] splittedLine = line.split(CSVSeparator);

                if (splittedLine.length != 2) {
                    throw new ChartDataException("Incorrect input data, expected " + 2 + " but there was " + splittedLine.length);
                }

                chartData.setData(splittedLine);
                printLine(splittedLine);
            }
        } catch (ChartDataException exc) {
            DialogFactory.showDialog(Alert.AlertType.ERROR, exc.getMessage());
            return null;
        }

        return chartData;
    }

    /**
     * Print readed from file line.
     *
     * @param line Single line from File.
     */
    private void printLine(String[] line){
        for(String str: line){
            System.out.print(str + " ");
        }
        System.out.println();
    }
}
