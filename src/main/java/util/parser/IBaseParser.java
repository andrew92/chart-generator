package util.parser;

import exception.AlertTypeException;
import model.ChartData;

import java.io.File;
import java.io.IOException;

/**
 * Base Parser Interface.
 *
 * Created by Andrzej Olkiewicz on 2016-05-15.
 */
public interface IBaseParser {

    /**
     * Parse data from file.
     *
     * @param data File to be parsed.
     * @param chartType Chart type.
     * @return ChartData instance contains parsed data.
     * @throws IOException
     * @throws AlertTypeException
     */
    ChartData parseData(File data, String chartType) throws IOException, AlertTypeException;


    /**
     * Save data to file.
     *
     * @param saveTo File to be saved.
     * @param chartData Data to be saved.
     * @throws AlertTypeException
     */
    void saveData(File saveTo, ChartData chartData) throws AlertTypeException;
}
