package util.factory;

import exception.AlertTypeException;
import exception.ChartTypeException;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.chart.*;
import javafx.scene.control.Alert;
import model.ChartData;
import model.Data;
import org.fxmisc.easybind.EasyBind;
import util.helper.ConstHelper;

import java.text.NumberFormat;
import java.text.ParseException;

/**
 * Chart Factory class.
 *
 * Created by Andrzej Olkiewicz on 2016-05-23.
 */
public class ChartFactoryImpl implements IChartFactory {

    /**
     * Returns ready to use chart instance. Chart type based on passed parameter.
     *
     * @param type Type of Chart
     * @param chartData Data that may be filled in chart
     * @return Chart instance based of declared chart type
     * @throws AlertTypeException Invalid Alert type
     */
    @Override
    public Chart getChart(String type, ChartData chartData) throws AlertTypeException {
        try {
            switch (type) {
                case ConstHelper.BarChart:
                    return createBarChart(chartData);
                case ConstHelper.PieChart:
                    return createPieChart(chartData);
                case ConstHelper.LineChart:
                    return createLineChart(chartData);
                case ConstHelper.AreaChart:
                    return createAreaChart(chartData);
                case ConstHelper.BubbleChart:
                    return createBubbleChart(chartData);
                case ConstHelper.ScatterChart:
                    return createScatterChart(chartData);
                default:
                    throw new ChartTypeException("Chart type: " + type + " cannot be instantiated");
            }
        } catch (ChartTypeException exc) {
            DialogFactory.showDialog(Alert.AlertType.ERROR, exc.getMessage());
            return null;
        }
    }

    /**
     * Private method, used by main Chart Factory method. Returns ready to use BarChart instance.
     *
     * @param chartData Data that maybe filled in chart
     * @return BarChart instance
     */
    private BarChart createBarChart(ChartData chartData) {
        final CategoryAxis xAxis = new CategoryAxis();
        final NumberAxis yAxis = new NumberAxis();

        final BarChart<String, Number> bc =
                new BarChart<String, Number>(xAxis, yAxis);

        bc.setTitle("Chart Title");
        xAxis.setLabel("XAxis Label Title");
        yAxis.setLabel("YAxis Label Title");

        // append Data
        fillChartWithData(chartData, bc);

        return bc;
    }

    /**
     * Private method, used by main Chart Factory method. Returns ready to use Pie Chart instance.
     *
     * @param chartData Data that maybe filled in chart
     * @return PieChart instance
     * @throws AlertTypeException
     */
    private PieChart createPieChart(ChartData chartData) throws AlertTypeException {
        final PieChart chart = new PieChart();
        chart.setTitle("Chart Title");

        ObservableList<PieChart.Data> pieChartData = FXCollections.observableArrayList();

        // append Data
        for (Data data : chartData.getData()) {
            if (data.getValues().size() > 1) {
                DialogFactory.showDialog(Alert.AlertType.ERROR, "Incorrect Data, more than one value for serie " + data.getName());
                return null;
            }

            pieChartData.add(new PieChart.Data(data.getName(), (Double) data.getFirstValue()));
        }

        chart.getData().addAll(pieChartData);

        return chart;
    }

    /**
     * Private method, used by main Chart Factory method. Returns ready to use LineChart instance.
     *
     * @param chartData Data that maybe filled in chart
     * @return LineChart instance
     */
    private LineChart createLineChart(ChartData chartData) {
        final CategoryAxis xAxis = new CategoryAxis();
        final NumberAxis yAxis = new NumberAxis();

        final LineChart<String, Number> bc =
                new LineChart<String, Number>(xAxis, yAxis);

        bc.setTitle("Chart Title");
        xAxis.setLabel("XAxis Label Title");
        yAxis.setLabel("YAxis Label Title");

        // append Data
        fillChartWithData(chartData, bc);

        return bc;
    }

    /**
     * Private method, used by main Chart Factory method. Returns ready to use AreaChart instance.
     *
     * @param chartData Data that maybe filled in chart
     * @return AreaChart<String, Number> instance
     */
    private AreaChart<String, Number> createAreaChart(ChartData chartData) {
        final CategoryAxis xAxis = new CategoryAxis();
        final NumberAxis yAxis = new NumberAxis();
        final AreaChart<String, Number> ac =
                new AreaChart<String, Number>(xAxis, yAxis);


        ac.setTitle("Chart Title");
        xAxis.setLabel("XAxis Label Title");
        yAxis.setLabel("YAxis Label Title");

        // append Data
        fillChartWithData(chartData, ac);

        return ac;
    }


    /**
     * Private method, used by main Chart Factory method. Returns ready to use BubbleChart instance.
     *
     * @param chartData Data that maybe filled in chart
     * @return BubbleChart instance
     * @throws AlertTypeException
     */
    @SuppressWarnings("unchecked")
    private BubbleChart createBubbleChart(ChartData chartData) throws AlertTypeException {
        final NumberAxis xAxis = new NumberAxis();
        final NumberAxis yAxis = new NumberAxis();
        final BubbleChart<Number, Number> ac =
                new BubbleChart<Number, Number>(xAxis, yAxis);


        ac.setTitle("Chart Title");
        xAxis.setLabel("XAxis Label Title");
        yAxis.setLabel("YAxis Label Title");

        // append Data
        if(chartData.getSeriesSize() != 0) {
            int count = 0;

            for (String series : chartData.getSeries()) {
                XYChart.Series chartSeries = new XYChart.Series();
                chartSeries.setName(series);

                for (Data data : chartData.getData())
                    try {
                        chartSeries.getData().add(new XYChart.Data<>(NumberFormat.getInstance().parse(data.getName()), data.getValue(count)));
                    } catch (ParseException e) {
                        DialogFactory.showDialog(Alert.AlertType.ERROR, "Incorrect Data, all values should be numbers, cannot parse " + data.getName());
                        return null;
                    }

                ac.getData().add(chartSeries);
                count++;
            }
        } else {
            for (Data data : chartData.getData()) {
                XYChart.Series chartSeries = new XYChart.Series();
                chartSeries.setName(data.getName());

                try {
                    chartSeries.getData().add(new XYChart.Data<>(NumberFormat.getInstance().parse(data.getName()), data.getFirstValue()));
                } catch (ParseException e) {
                    DialogFactory.showDialog(Alert.AlertType.ERROR, "Incorrect Data, all values should be numbers, cannot parse " + data.getName());
                    return null;
                }


                ac.getData().add(chartSeries);
            }
        }

        return ac;
    }

    /**
     * Private method, used by main Chart Factory method. Returns ready to use ScatterChart instance.
     *
     * @param chartData Data that maybe filled in chart
     * @return ScatterChart
     * @throws AlertTypeException
     */
    @SuppressWarnings("unchecked")
    private ScatterChart createScatterChart(ChartData chartData) throws AlertTypeException {
        final CategoryAxis xAxis = new CategoryAxis();
        final NumberAxis yAxis = new NumberAxis();
        final ScatterChart<String,Number> sc = new
                ScatterChart<>(xAxis,yAxis);

        xAxis.setLabel("Age (years)");
        yAxis.setLabel("Returns to date");
        sc.setTitle("Investment Overview");

        // append Data
        if(chartData.getSeriesSize() != 0) {
            int count = 0;

            for (String series : chartData.getSeries()) {
                XYChart.Series chartSeries = new XYChart.Series();
                chartSeries.setName(series);

                for (Data data : chartData.getData())
                        chartSeries.getData().add(new XYChart.Data<>(data.getName(), data.getValue(count)));

                sc.getData().add(chartSeries);
                count++;
            }
        } else {
            for (Data data : chartData.getData()) {
                XYChart.Series chartSeries = new XYChart.Series();
                chartSeries.setName(data.getName());

                    chartSeries.getData().add(new XYChart.Data<>(data.getName(), data.getFirstValue()));

                sc.getData().add(chartSeries);
            }
        }

        return sc;
    }


    /**
     * Private method, used to pupulate chart with data.
     *
     * @param chartData Data that maybe filled in chart
     * @param chart Chart instance that will be filled with data
     */
    private void fillChartWithData(ChartData chartData, XYChart chart){
        chartData.setBindings();

        if (chartData.getSeriesSize() != 0)
            fillChartWithDataSeriesNotEmpty(chartData, chart);
        else
            fillChartWithDataSeriesEmpty(chartData, chart);
    }


    /**
     * Private method, used to to pupulate chart with data when Data Series are available, e.g. BarChart.
     *
     * @param chartData Data that maybe filled in chart
     * @param chart Chart instance that will be filled with data
     */
    @SuppressWarnings("unchecked")
    private void fillChartWithDataSeriesNotEmpty(ChartData chartData, XYChart chart) {
        int count = 0;

        for (String series : chartData.getSeries()) {
            final int finalCount = count;

            ObservableList<XYChart.Data<String, Number>> dataSet1 = EasyBind.map(chartData.getData(),
                    item -> new XYChart.Data<>(item.getName(), (Number) item.getValue(finalCount)));

            XYChart.Series<String, Number> chartSeries = new XYChart.Series<>(series, dataSet1);

            chart.getData().add(chartSeries);
            count++;
        }
    }


    /**
     * Private method, used to to pupulate chart with data when Data Series are not available, e.g. PieChart.
     *
     * @param chartData Data that maybe filled in chart
     * @param chart Chart instance that will be filled with data
     */
    @SuppressWarnings("unchecked")
    private void fillChartWithDataSeriesEmpty(ChartData chartData, XYChart chart) {
            ObservableList<XYChart.Data<String, Number>> dataSet1 = EasyBind.map(chartData.getData(),
                    item -> new XYChart.Data<>(item.getName(), (Number) item.getFirstValue()));

            XYChart.Series<String, Number> chartSeries = new XYChart.Series<>(null, dataSet1);

            chart.getData().add(chartSeries);
    }
}
