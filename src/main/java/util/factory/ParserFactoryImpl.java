package util.factory;

import exception.AlertTypeException;
import exception.FileExtensionException;
import javafx.scene.control.Alert;
import util.helper.ConstHelper;
import util.parser.CSVParserImpl;
import util.parser.IBaseParser;

/**
 * Parser Factory class
 *
 * Created by Andrzej Olkiewicz on 2016-05-15.
 */
public class ParserFactoryImpl extends IParserFactory{

    /**
     * Returns ready to use parser based on file extension.
     *
     * @param fileExtension File Extension e.g. csv
     * @return Parser based on file extension
     * @throws AlertTypeException
     */
    @Override
    public IBaseParser getFileParser(String fileExtension) throws AlertTypeException {
        try {
            switch (fileExtension) {
                case ConstHelper.CSVExtension:
                    return CSVParserImpl.getInstance();
                default:
                    throw new FileExtensionException("File Extension: " + fileExtension + " cannot be instantiated");
            }
        } catch (FileExtensionException exc){
            DialogFactory.showDialog(Alert.AlertType.ERROR, exc.getMessage());
            return null;
        }
    }
}
