package util.factory;

import exception.AlertTypeException;
import exception.FileExtensionException;
import util.parser.IBaseParser;

/**
 * Base Parser Factory Interface.
 *
 * Created by Andrzej Olkiewicz on 2016-05-15.
 */
public abstract class IParserFactory {

    /**
     * @param fileExtension File Extension e.g. csv
     * @return Ready to use Parser based on file extension
     * @throws FileExtensionException Invalid type extension
     * @throws AlertTypeException Invalid alert type
     */
    public abstract IBaseParser getFileParser(String fileExtension) throws FileExtensionException, AlertTypeException;
}
