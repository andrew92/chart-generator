package util.factory;

import exception.AlertTypeException;
import exception.ChartTypeException;
import javafx.scene.chart.Chart;
import model.ChartData;

/**
 * Base Chart Factory Interface
 *
 * Created by Andrzej Olkiewicz on 2016-05-23.
 */
public interface IChartFactory {

    /**
     * @param type Type of Chart
     * @param chartData Data that may be filled in chart
     * @return Chart instance based of declared chart type
     * @throws ChartTypeException Invalid Chart Type
     * @throws AlertTypeException Invalid Alert Type
     */
    Chart getChart(String type, ChartData chartData) throws ChartTypeException, AlertTypeException;
}
