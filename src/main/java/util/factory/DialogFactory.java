package util.factory;

import exception.AlertTypeException;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;

/**
 * Dialog Factory class.
 *
 * Created by Andrzej Olkiewicz on 2016-05-30.
 */
public abstract class DialogFactory {

    /**
     * Shows ready Dialog with passed as argument message, based on alert type.
     *
     * @param type Type of Alert.
     * @param message Message to be displayed.
     * @throws AlertTypeException
     */
    public static void showDialog(AlertType type, String message) throws AlertTypeException {
        switch(type){
            case INFORMATION:
                showInformationDialog(message);
                return;
            case WARNING:
                showWarningDialog(message);
                return;
            case ERROR:
                showErrorDialog(message);
                return;
            default:
                throw new AlertTypeException("Alert type: " + type + " cannot be instantiated");
        }
    }

    /**
     * Shows Information Dialog.
     *
     * @param message Message to be displayed.
     */
    private static void showInformationDialog(String message){
        Alert alert = new Alert(AlertType.INFORMATION);
        alert.setTitle("Information Dialog");
        alert.setHeaderText(null);
        alert.setContentText(message);

        alert.showAndWait();
    }

    /**
     * Shows Warning Dialog.
     *
     * @param message Message to be displayed.
     */
    private static void showWarningDialog(String message){
        Alert alert = new Alert(AlertType.WARNING);
        alert.setTitle("Warning Dialog");
        alert.setHeaderText(null);
        alert.setContentText(message);

        alert.showAndWait();
    }


    /**
     * Shows Error Dialog.
     *
     * @param message Message to be displayed.
     */
    private static void showErrorDialog(String message){
        Alert alert = new Alert(AlertType.ERROR);
        alert.setTitle("Error Dialog");
        alert.setHeaderText(null);
        alert.setContentText(message);

        alert.showAndWait();
    }
}
