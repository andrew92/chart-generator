package exception;

/**
 * Exception when Chart Type is invalid.
 *
 * Created by Andrzej Olkiewicz on 2016-05-23.
 */
public class ChartTypeException extends Exception {

    public ChartTypeException(String message){
        super(message);
    }
}
