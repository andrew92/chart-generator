package exception;

/**
 * Exception when Alert type is invalid.
 *
 * Created by Andrzej Olkiewicz on 2016-05-30.
 */
public class AlertTypeException extends Exception {

    public AlertTypeException(String message){
        super(message);
    }
}
