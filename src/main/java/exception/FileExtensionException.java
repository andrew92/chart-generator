package exception;

/**
 * Exception when File Extension is invalid.
 *
 * Created by Andrzej Olkiewicz on 2016-05-15.
 */
public class FileExtensionException extends Exception {
    public FileExtensionException(String message){
        super(message);
    }
}
