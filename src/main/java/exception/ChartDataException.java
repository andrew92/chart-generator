package exception;

/**
 * Exception when ChartData is invalid.
 *
 * Created by Andrzej Olkiewicz on 2016-05-15.
 */
public class ChartDataException extends Exception {
    public ChartDataException(String message){
        super(message);
    }
}
