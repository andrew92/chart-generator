package model;

import javafx.beans.property.Property;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

/**
 * Represents single data record.
 *
 * Created by Andrzej Olkiewicz on 2016-05-16.
 */
public class Data {

    private final StringProperty name = new SimpleStringProperty(this, "name");
    private final ObservableList<Property<Number>> values = FXCollections.observableArrayList();


    public ObservableList<Property<Number>> getValues() {
        return values;
    }

    public String getName() {
        return name.get();
    }

    public final StringProperty nameProperty() {
        return name;
    }

    public void setName(String name) {
        this.name.set(name);
    }

    public void addValue(Double val) {
        values.add(new SimpleDoubleProperty(val));
    }

    public Object getFirstValue() {
        return values.get(0).getValue();
    }

    public Object getValue(int count) {
        return values.get(count).getValue();
    }

    public Property<Number> getProperty(int count){
        return this.values.get(count);
    }

}
