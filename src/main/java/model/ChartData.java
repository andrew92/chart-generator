package model;

import javafx.beans.Observable;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

/**
 * Represents all data imported from file.
 *
 * Created by Andrzej Olkiewicz on 2016-05-15.
 */
public class ChartData {
    private ObservableList<String> series;
    private ObservableList<Data> data;

    public ChartData(){
        series = FXCollections.observableArrayList();
        data = FXCollections.observableArrayList();
    }

    public void setSeries(String[] series){
        this.series.addAll(series);
    }

    public ObservableList<String> getSeries() {
        return series;
    }

    public int getSeriesSize(){
        return series.size();
    }

    public void setData(String[] data){
        boolean firstVal = true;
        Data newData = new Data();

        for(String str: data){
            if(firstVal) {
                newData.setName(str);
                firstVal = false;
                continue;
            }

            try {
                Double val = Double.parseDouble(str);
                newData.addValue(val);
            } catch (NumberFormatException nfe){
                nfe.printStackTrace();
            }
        }

        this.data.add(newData);
    }

    public ObservableList<Data> getData(){
        return data;
    }

    public int getDataValuesSize(){
        return data.get(0).getValues().size();
    }

    public void setData(ObservableList<Data> data) {
        this.data = data;
    }

    public void setBindings(){

        ObservableList<Data> tempData = FXCollections.observableArrayList(item -> {
            Observable[] obs = new Observable[item.getValues().size() + 1];
            obs[0] = item.nameProperty();

            for(int i = 0; i < item.getValues().size(); i++){
                obs[i+1] = item.getProperty(i);
            }

            return obs;
        });

        tempData.addAll(data);
        setData(tempData);
    }

    @Override
    public String toString() {
        return "ChartData{" +
                "series=" + series +
                ", data=" + data +
                '}';
    }
}
