import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

import java.io.IOException;

/**
 * Main JavaFX Application class.
 *
 * Created by Andrzej Olkiewicz on 2016-04-24.
 */

public class Main extends Application {

    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage primaryStage) throws IOException {
        initGUI(primaryStage);
    }

    /**
     * Init GUI before start.
     *
     * @param stage
     * @throws IOException
     */
    private void initGUI(Stage stage) throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("/views/main.fxml"));

        Scene scene = new Scene(root);
        scene.getStylesheets().addAll("/css/style.css");

        stage.setTitle("ChartGenerator");
        stage.setResizable(false);
        stage.setScene(scene);
        stage.show();
    }
}
