package util.parser;

import model.ChartData;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.*;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.powermock.reflect.internal.WhiteboxImpl;
import util.helper.ConstHelper;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.PrintWriter;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;
import static org.mockito.Mockito.times;
import static org.powermock.api.mockito.PowerMockito.when;

/**
 * Created by Andrzej Olkiewicz on 2016-06-11.
 */
@RunWith(PowerMockRunner.class)
@PrepareForTest({CSVParserImpl.class, StringBuilder.class })
public class CSVParserImplLightTest {

    CSVParserImpl csvParser = CSVParserImpl.getInstance();

    @Mock
    File mockFile;

    @Mock
    BufferedReader mockBufferedReader;

    @Mock
    FileReader mockFileReader;

    @Mock
    PrintWriter mockPrintWriter;

    @Spy
    ChartData chartDataSpy = new ChartData();

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);

        // Mock Constructor
        PowerMockito.whenNew(FileReader.class).withArguments(mockFile).thenReturn(mockFileReader);
        PowerMockito.whenNew(BufferedReader.class).withArguments(mockFileReader).thenReturn(mockBufferedReader);
        PowerMockito.whenNew(ChartData.class).withNoArguments().thenReturn(chartDataSpy);
        PowerMockito.whenNew(PrintWriter.class).withArguments(mockFile).thenReturn(mockPrintWriter);
    }

    @Test
    public void shouldInvokeParseDataWhenSeriesArePresentWhenChartTypeIsBar() throws Exception {
        // GIVEN
        String chartType = ConstHelper.BarChart;

        // WHEN
        csvParser.parseData(mockFile, chartType);

        // THEN
        PowerMockito.verifyPrivate(csvParser, times(1)).invoke("parseDataWhenSeriesArePresent", mockFile);
    }

    @Test
    public void shouldInvokeParseDataWhenSeriesArePresentWhenChartTypeIsLine() throws Exception {
        // GIVEN
        String chartType = ConstHelper.LineChart;

        // WHEN
        csvParser.parseData(mockFile, chartType);

        // THEN
        PowerMockito.verifyPrivate(csvParser, times(1)).invoke("parseDataWhenSeriesArePresent", mockFile);
    }

    @Test
    public void shouldInvokeParseDataWhenSeriesArePresentWhenChartTypeIsArea() throws Exception {
        // GIVEN
        String chartType = ConstHelper.AreaChart;

        // WHEN
        csvParser.parseData(mockFile, chartType);

        // THEN
        PowerMockito.verifyPrivate(csvParser, times(1)).invoke("parseDataWhenSeriesArePresent", mockFile);
    }

    @Test
    public void shouldInvokeParseDataWhenSeriesArePresentWhenChartTypeIsBubble() throws Exception {
        // GIVEN
        String chartType = ConstHelper.BubbleChart;

        // WHEN
        csvParser.parseData(mockFile, chartType);

        // THEN
        PowerMockito.verifyPrivate(csvParser, times(1)).invoke("parseDataWhenSeriesArePresent", mockFile);
    }

    @Test
    public void shouldInvokeParseDataWhenSeriesArePresentWhenChartTypeIsScatter() throws Exception {
        // GIVEN
        String chartType = ConstHelper.ScatterChart;

        // WHEN
        csvParser.parseData(mockFile, chartType);

        // THEN
        PowerMockito.verifyPrivate(csvParser, times(1)).invoke("parseDataWhenSeriesArePresent", mockFile);
    }

    @Test
    public void shouldInvokeParseDataWhenSeriesAreNotPresentWhenChartTypeIsPie() throws Exception {
        // GIVEN
        String chartType = ConstHelper.PieChart;

        // WHEN
        csvParser.parseData(mockFile, chartType);

        // THEN
        PowerMockito.verifyPrivate(csvParser, times(1)).invoke("parseDataWhenSeriesAreNotPresent", mockFile);
    }

    @Test
    public void shouldSetDataWhenSeriesAreAvailable() throws Exception {
        // GIVEN
        String line1 = "2003,2004,2005";
        String line2 = "Austria,500,600,700";

        when(mockBufferedReader.readLine()).thenReturn(line1, line2, null);

        // WHEN
        WhiteboxImpl.invokeMethod(csvParser, "parseDataWhenSeriesArePresent", mockFile);

        // THEN
        InOrder inOrder = Mockito.inOrder(chartDataSpy);
        inOrder.verify(chartDataSpy).setSeries(line1.split(","));
        inOrder.verify(chartDataSpy).setData(line2.split(","));
    }

    @Test
    public void shouldSetDataWhenSeriesAreNotAvailable() throws Exception {
        // GIVEN
        String line1 = "Sunday,100.0";
        String line2 = "Monday,200.0";

        when(mockBufferedReader.readLine()).thenReturn(line1, line2, null);

        // WHEN
        WhiteboxImpl.invokeMethod(csvParser, "parseDataWhenSeriesAreNotPresent", mockFile);

        // THEN
        InOrder inOrder = Mockito.inOrder(chartDataSpy);
        inOrder.verify(chartDataSpy).setData(line1.split(","));
        inOrder.verify(chartDataSpy).setData(line2.split(","));
    }

    @Test
    public void shouldSaveData() throws Exception {
        // GIVEN
        StringBuilder stringBuilder = new StringBuilder();
        StringBuilder mockStringBuilder = PowerMockito.spy(stringBuilder);

        PowerMockito.whenNew(StringBuilder.class).withNoArguments().thenReturn(mockStringBuilder);

        chartDataSpy.setSeries(new String[]{"2003", "2004", "2005"});
        chartDataSpy.setData(new String[]{"Austria", "500.0", "600.0", "700.0"});
        chartDataSpy.setData(new String[]{"Brazil", "500.0", "600.0", "701.0"});
        chartDataSpy.setData(new String[]{"France", "500.0", "600.0", "702.0"});
        chartDataSpy.setData(new String[]{"Italy", "500.0", "600.0", "703.0"});
        chartDataSpy.setData(new String[]{"Usa", "500.0", "600.0", "704.0"});

        String result = "2003,2004,2005\n" +
                "Austria,500.0,600.0,700.0\n" +
                "Brazil,500.0,600.0,701.0\n" +
                "France,500.0,600.0,702.0\n" +
                "Italy,500.0,600.0,703.0\n" +
                "Usa,500.0,600.0,704.0\n";

        // WHEN
        csvParser.saveData(mockFile, chartDataSpy);

        // THEN
        assertThat(mockStringBuilder.toString(), is(result));

        Mockito.verify(mockPrintWriter, times(1)).write(mockStringBuilder.toString());
    }
}