package util.helper;

import exception.ChartTypeException;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.powermock.modules.junit4.PowerMockRunner;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;

/**
 * Created by Andrzej Olkiewicz on 2016-06-11.
 */
@RunWith(PowerMockRunner.class)
public class ChartCSSHelperLightTest {

    @Rule
    ExpectedException expectedException = ExpectedException.none();

    @Before
    public void setUp() throws Exception {
    }

    @Test
    public void shouldReturnDefColorBarChartWhenChartTypeIsBarChart() throws Exception {
        // GIVEN
        String chartType = ConstHelper.BarChart;

        // WHEN
        String def = ChartCSSHelper.selectColorDef(chartType);

        // THEN
        assertThat(def, is(ConstHelper.DefColorBarChart));
    }

    @Test
    public void shouldReturnDefColorPieChartWhenChartTypeIsPieChart() throws Exception {
        // GIVEN
        String chartType = ConstHelper.PieChart;

        // WHEN
        String def = ChartCSSHelper.selectColorDef(chartType);

        // THEN
        assertThat(def, is(ConstHelper.DefColorPieChart));
    }

    @Test
    public void shouldReturnDefColorLineChartWhenChartTypeIsLineChart() throws Exception {
        // GIVEN
        String chartType = ConstHelper.LineChart;

        // WHEN
        String def = ChartCSSHelper.selectColorDef(chartType);

        // THEN
        assertThat(def, is(ConstHelper.DefColorLineChart));
    }

    @Test
    public void shouldReturnDefColorAreaChartLineWhenChartTypeIsAreaChart() throws Exception {
        // GIVEN
        String chartType = ConstHelper.AreaChart;

        // WHEN
        String def = ChartCSSHelper.selectColorDef(chartType);

        // THEN
        assertThat(def, is(ConstHelper.DefColorAreaChartLine));
    }

    @Test
    public void shouldReturnDefColorBubbleChartWhenChartTypeIsBubbleChart() throws Exception {
        // GIVEN
        String chartType = ConstHelper.BubbleChart;

        // WHEN
        String def = ChartCSSHelper.selectColorDef(chartType);

        // THEN
        assertThat(def, is(ConstHelper.DefColorBubbleChart));
    }

    @Test
    public void shouldThrowExceptionWhenChartTypeIsInvalidAndSelectColorDefMethod() throws Exception {
        // GIVEN
        String chartType = "InvalidChartType";
        expectedException.expect(ChartTypeException.class);
        expectedException.expectMessage("Chart type: " + chartType + " is invalid");

        // WHEN
        ChartCSSHelper.selectColorDef(chartType);

        // THEN
    }

    @Test
    public void shouldReturnDefColorBarChartFillWhenChartTypeIsBarChart() throws Exception {
        // GIVEN
        String chartType = ConstHelper.BarChart;

        // WHEN
        String def = ChartCSSHelper.selectColorFill(chartType);

        // THEN
        assertThat(def, is(ConstHelper.DefColorBarChartFill));
    }

    @Test
    public void shouldReturnDefColorPieChartFillWhenChartTypeIsPieChart() throws Exception {
        // GIVEN
        String chartType = ConstHelper.PieChart;

        // WHEN
        String def = ChartCSSHelper.selectColorFill(chartType);

        // THEN
        assertThat(def, is(ConstHelper.DefColorPieChartFill));
    }

    @Test
    public void shouldReturnDefColorLineChartFillWhenChartTypeIsLineChart() throws Exception {
        // GIVEN
        String chartType = ConstHelper.LineChart;

        // WHEN
        String def = ChartCSSHelper.selectColorFill(chartType);

        // THEN
        assertThat(def, is(ConstHelper.FX_STROKE_0));
    }

    @Test
    public void shouldReturnDefColorAreaChartLineFillWhenChartTypeIsAreaChart() throws Exception {
        // GIVEN
        String chartType = ConstHelper.AreaChart;

        // WHEN
        String def = ChartCSSHelper.selectColorFill(chartType);

        // THEN
        assertThat(def, is(ConstHelper.DefColorAreaChartLineFill));
    }

    @Test
    public void shouldReturnDefColorBubbleChartFillWhenChartTypeIsBubbleChart() throws Exception {
        // GIVEN
        String chartType = ConstHelper.BubbleChart;

        // WHEN
        String def = ChartCSSHelper.selectColorFill(chartType);

        // THEN
        assertThat(def, is(ConstHelper.DefColorBubbleChartFill));
    }

    @Test
    public void shouldThrowExceptionWhenChartTypeIsInvalidAndSelectColorFillMethod() throws Exception {
        // GIVEN
        String chartType = "InvalidChartType";

        expectedException.expect(ChartTypeException.class);
        expectedException.expectMessage("Chart type: " + chartType + " is invalid");

        // WHEN
        ChartCSSHelper.selectColorFill(chartType);

        // THEN
    }

    @Test
    public void shouldReturnDefColorAreaChartFillWhenChartTypeIsAreaChart() throws Exception {
        // GIVEN
        String chartType = ConstHelper.AreaChart;

        // WHEN
        String def = ChartCSSHelper.selectAreaFillColorDef(chartType);

        // THEN
        assertThat(def, is(ConstHelper.DefColorAreaChartFill));
    }

    @Test
    public void shouldThrowExceptionWhenChartTypeIsInvalidAndSelectAreaFillColorDefMethod() throws Exception {
        // GIVEN
        String chartType = "InvalidChartType";

        expectedException.expect(ChartTypeException.class);
        expectedException.expectMessage("Chart type: " + chartType + " is invalid");

        // WHEN
        ChartCSSHelper.selectAreaFillColorDef(chartType);

        // THEN
    }

    @Test
    public void shouldReturnDefColorAreaChartFillColorWhenChartTypeIsAreaChart() throws Exception {
        // GIVEN
        String chartType = ConstHelper.AreaChart;

        // WHEN
        String def = ChartCSSHelper.selectAreaFillColor(chartType);

        // THEN
        assertThat(def, is(ConstHelper.FX_FILL_0));
    }

    @Test
    public void shouldThrowExceptionWhenChartTypeIsInvalidAndSelectAreaFillColorMethod() throws Exception {
        // GIVEN
        String chartType = "InvalidChartType";

        expectedException.expect(ChartTypeException.class);
        expectedException.expectMessage("Chart type: " + chartType + " is invalid");

        // WHEN
        ChartCSSHelper.selectAreaFillColor(chartType);

        // THEN
    }
}