package util.helper;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.modules.junit4.PowerMockRunner;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;

/**
 * Created by Andrzej Olkiewicz on 2016-06-11.
 */
@RunWith(PowerMockRunner.class)
public class FileHelperLightTest {

    @Before
    public void setUp() throws Exception {
    }

    @Test
    public void shouldReturnCSVWhenFileExtensionIsCSV() throws Exception {
        // GIVEN
        String fileName = "sampledata-BarChart.csv";

        // THEN
        assertThat(FileHelper.getFileExtension(fileName), is("csv"));
    }
}