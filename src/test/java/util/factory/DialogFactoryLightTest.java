package util.factory;

import exception.AlertTypeException;
import javafx.scene.control.Alert;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import static org.mockito.Mockito.times;

/**
 * Created by Andrzej Olkiewicz on 2016-06-11.
 */
@RunWith(PowerMockRunner.class)
@PrepareForTest({DialogFactory.class, Alert.class})
public class DialogFactoryLightTest {

    @Rule
    public JavaFXThreadingRule javafxRule = new JavaFXThreadingRule();

    @Rule
    ExpectedException expectedException = ExpectedException.none();

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
    }

    private static String message = "DialogMessage";

    @Test
    public void shouldCreateAndShowWarningDialog() throws Exception {
        // GIVEN
        Alert.AlertType alertType = Alert.AlertType.WARNING;

        Alert mockAlert = PowerMockito.mock(Alert.class);

        // Mock Constructor
        PowerMockito.whenNew(Alert.class).withAnyArguments().thenReturn(mockAlert);

        // WHEN
        DialogFactory.showDialog(alertType, message);

        // THEN
        Mockito.verify(mockAlert, times(1)).setTitle("Warning Dialog");
        Mockito.verify(mockAlert, times(1)).setHeaderText(null);
        Mockito.verify(mockAlert, times(1)).setContentText(message);
        Mockito.verify(mockAlert, times(1)).showAndWait();
    }

    @Test
    public void shouldCreateAndShowInformationDialog() throws Exception {
        // GIVEN
        Alert.AlertType alertType = Alert.AlertType.INFORMATION;

        Alert mockAlert = PowerMockito.mock(Alert.class);

        // Mock Constructor
        PowerMockito.whenNew(Alert.class).withAnyArguments().thenReturn(mockAlert);

        // WHEN
        DialogFactory.showDialog(alertType, message);

        // THEN
        Mockito.verify(mockAlert, times(1)).setTitle("Information Dialog");
        Mockito.verify(mockAlert, times(1)).setHeaderText(null);
        Mockito.verify(mockAlert, times(1)).setContentText(message);
        Mockito.verify(mockAlert, times(1)).showAndWait();
    }

    @Test
    public void shouldCreateAndShowErrorDialog() throws Exception {
        // GIVEN
        Alert.AlertType alertType = Alert.AlertType.ERROR;

        Alert mockAlert = PowerMockito.mock(Alert.class);

        // Mock Constructor
        PowerMockito.whenNew(Alert.class).withAnyArguments().thenReturn(mockAlert);

        // WHEN
        DialogFactory.showDialog(alertType, message);

        // THEN
        Mockito.verify(mockAlert, times(1)).setTitle("Error Dialog");
        Mockito.verify(mockAlert, times(1)).setHeaderText(null);
        Mockito.verify(mockAlert, times(1)).setContentText(message);
        Mockito.verify(mockAlert, times(1)).showAndWait();
    }

    @Test
    public void shouldThrowExceptionWhenAlertTypeIsInvalid() throws Exception {
        // GIVEN
        Alert.AlertType alertType = Alert.AlertType.NONE;

        expectedException.expect(AlertTypeException.class);
        expectedException.expectMessage("Alert type: " + alertType + " cannot be instantiated");

        // WHEN
        DialogFactory.showDialog(alertType, message);

        // THEN
    }
}