package util.factory;

import javafx.scene.control.Alert;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import util.helper.ConstHelper;
import util.parser.CSVParserImpl;
import util.parser.IBaseParser;

import static org.hamcrest.core.Is.is;
import static org.hamcrest.core.IsNull.nullValue;
import static org.junit.Assert.assertThat;
import static org.powermock.api.mockito.PowerMockito.when;

/**
 * Created by Andrzej Olkiewicz on 2016-06-11.
 */
@RunWith(PowerMockRunner.class)
@PrepareForTest({DialogFactory.class, CSVParserImpl.class})
public class ParserFactoryImplLightTest {

    ParserFactoryImpl parserFactory;

    @Mock
    CSVParserImpl mockCSVParser;

    @Rule
    ExpectedException expectedException = ExpectedException.none();

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);

        parserFactory = new ParserFactoryImpl();

        // Mock Static
        PowerMockito.mockStatic(DialogFactory.class);

        PowerMockito.mockStatic(CSVParserImpl.class);
        when(CSVParserImpl.getInstance()).thenReturn(mockCSVParser);
    }

    @Test
    public void shouldReturnCSVParserWhenFileExtensionIsCSV() throws Exception {
        // GIVEN
        String fileExtension = ConstHelper.CSVExtension;

        // WHEN
        IBaseParser parser = parserFactory.getFileParser(fileExtension);

        // THEN
        assertThat(parser, is(mockCSVParser));
    }

    @Test
    public void shouldShowErrorDialogAndReturnNullWhenFileExtensioIsInvalid() throws Exception {
        // GIVEN
        String fileExtension = "InvalidExtension";

        // WHEN
        IBaseParser parser = parserFactory.getFileParser(fileExtension);

        // THEN
        PowerMockito.verifyStatic();
        DialogFactory.showDialog(Alert.AlertType.ERROR, "File Extension: " + fileExtension + " cannot be instantiated");

        assertThat(parser, is(nullValue()));
    }
}