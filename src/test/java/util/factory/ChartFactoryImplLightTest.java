package util.factory;

import javafx.scene.chart.*;
import model.ChartData;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import util.helper.ConstHelper;

import static org.hamcrest.CoreMatchers.instanceOf;
import static org.hamcrest.MatcherAssert.assertThat;

/**
 * Created by Andrzej Olkiewicz on 2016-06-11.
 */
@RunWith(PowerMockRunner.class)
@PrepareForTest({ })
public class ChartFactoryImplLightTest {

    @Rule
    public JavaFXThreadingRule javafxRule = new JavaFXThreadingRule();

    ChartFactoryImpl chartFactory = new ChartFactoryImpl();
    ChartData chartData = new ChartData();

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void shouldReturnBarChartInstance() throws Exception {
        // GIVEN
        String chartType = ConstHelper.BarChart;

        // WHEN
        Chart chart = chartFactory.getChart(chartType, chartData);

        // THEN
        assertThat(chart, instanceOf(BarChart.class));
    }

    @Test
    public void shouldReturnPieChartInstance() throws Exception {
        // GIVEN
        String chartType = ConstHelper.PieChart;

        // WHEN
        Chart chart = chartFactory.getChart(chartType, chartData);

        // THEN
        assertThat(chart, instanceOf(PieChart.class));
    }

    @Test
    public void shouldReturnLineChartInstance() throws Exception {
        // GIVEN
        String chartType = ConstHelper.LineChart;

        // WHEN
        Chart chart = chartFactory.getChart(chartType, chartData);

        // THEN
        assertThat(chart, instanceOf(LineChart.class));
    }

    @Test
    public void shouldReturnAreaChartInstance() throws Exception {
        // GIVEN
        String chartType = ConstHelper.AreaChart;

        // WHEN
        Chart chart = chartFactory.getChart(chartType, chartData);

        // THEN
        assertThat(chart, instanceOf(AreaChart.class));
    }

    @Test
    public void shouldReturnBubbleChartInstance() throws Exception {
        // GIVEN
        String chartType = ConstHelper.BubbleChart;

        // WHEN
        Chart chart = chartFactory.getChart(chartType, chartData);

        // THEN
        assertThat(chart, instanceOf(BubbleChart.class));
    }

    @Test
    public void shouldReturnScatterChartInstance() throws Exception {
        // GIVEN
        String chartType = ConstHelper.ScatterChart;

        // WHEN
        Chart chart = chartFactory.getChart(chartType, chartData);

        // THEN
        assertThat(chart, instanceOf(ScatterChart.class));
    }
}